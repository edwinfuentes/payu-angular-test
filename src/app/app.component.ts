import { Component } from '@angular/core';
import { environment } from '../environments/environment';


declare var payU: any;


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public response = {};

    generateToken() {
        try {
            payU.setURL(`${environment.urlTokenPayu}`);
            payU.setPublicKey(`${environment.publicKeyPayu}`);
            payU.setAccountID(`${environment.accountIdPayu}`);
            payU.setListBoxID('payUlistID');
            payU.setLanguage('es');
            payU.getPaymentMethods();
            console.log('(AppComponent) -----> generateToken | PayU setup completed.');
            payU.setCardDetails({
                number: '4111111111111111',
                method: 'VISA',
                exp_year: 2020,
                exp_month: 7,
                cvv: '123',
                name_card: 'APPROVED',
                payer_id: 17176,
                document: '15645844'
            });
            payU.createToken(
                response => {
                    console.log('(AppComponent) -----> generateToken | createToken response: ', response);
                },
                error => {
                    console.log('(AppComponent) -----> generateToken | createToken error: ', error);
                }
            );
        } catch (e) {
            console.log('(AppComponent) -----> generateToken | Error: ', e);
        }
    }
}
